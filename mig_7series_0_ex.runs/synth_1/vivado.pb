
y
Command: %s
53*	vivadotcl2H
4synth_design -top example_top -part xc7a100tcsg324-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a100t2default:defaultZ17-349h px� 
�
Pparameter declaration becomes local in %s with formal parameter declaration list2507*oasys29
%mig_7series_v4_1_init_mem_pattern_ctr2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
3572default:default8@Z8-2507h px� 
�
%s*synth2�
�Starting RTL Elaboration : Time (s): cpu = 00:00:02 ; elapsed = 00:00:02 . Memory (MB): peak = 1372.449 ; gain = 83.223 ; free physical = 1925 ; free virtual = 6355
2default:defaulth px� 
�
synthesizing module '%s'%s4497*oasys2
example_top2default:default2
 2default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
752default:default8@Z8-6157h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
2672default:default8@Z8-6104h px� 
c
%s
*synth2K
7	Parameter PORT_MODE bound to: BI_MODE - type: string 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter TST_MEM_INSTR_MODE bound to: R_W_INSTR_MODE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CMD_PATTERN bound to: CGEN_ALL - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CMD_WDT bound to: 1023 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter WR_WDT bound to: 8191 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RD_WDT bound to: 1023 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter BEGIN_ADDRESS bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter END_ADDRESS bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter PRBS_EADDR_MASK_POS bound to: -16777216 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter CK_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter nCS_PER_RANK bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter CKE_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DM_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ODT_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter BANK_WIDTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter COL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter CS_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DQ_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter DQS_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter DQS_CNT_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter DRAM_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ECC bound to: OFF - type: string 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ECC_TEST bound to: OFF - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nBANK_MACHS bound to: 4 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter RANKS bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter ROW_WIDTH bound to: 14 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 28 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BURST_MODE bound to: 8 - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter CLKIN_PERIOD bound to: -2147483647 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter CLKFBOUT_MULT bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter DIVCLK_DIVIDE bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter CLKOUT0_PHASE bound to: 0.000000 - type: float 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CLKOUT0_DIVIDE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CLKOUT1_DIVIDE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CLKOUT2_DIVIDE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CLKOUT3_DIVIDE bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MMCM_VCO bound to: 620 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter MMCM_MULT_F bound to: 4 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter MMCM_DIVCLK_DIVIDE bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SIMULATION bound to: FALSE - type: string 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter DRAM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter DEBUG_PORT bound to: OFF - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter RST_ACT_LOW bound to: 1 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter RANK_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PAYLOAD_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter BURST_LENGTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter APP_DATA_WIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter APP_MASK_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter TG_ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter MASK_SIZE bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2!
mig_7series_02default:default2
 2default:default2�
�/home/davide/vivadoPrjects/mig_7series_0_ex/mig_7series_0_ex.runs/synth_1/.Xil/Vivado-29999-davide-N552VX/realtime/mig_7series_0_stub.v2default:default2
52default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
mig_7series_02default:default2
 2default:default2
12default:default2
12default:default2�
�/home/davide/vivadoPrjects/mig_7series_0_ex/mig_7series_0_ex.runs/synth_1/.Xil/Vivado-29999-davide-N552VX/realtime/mig_7series_0_stub.v2default:default2
52default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys24
 mig_7series_v4_1_traffic_gen_top2default:default2
 2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
782default:default8@Z8-6157h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2692default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2692default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2692default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SIMULATION bound to: FALSE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX7 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter TST_MEM_INSTR_MODE bound to: R_W_INSTR_MODE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter MEM_COL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter DATA_WIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter MASK_SIZE bound to: 4 - type: integer 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter BEGIN_ADDRESS bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter END_ADDRESS bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter PRBS_EADDR_MASK_POS bound to: -16777216 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter CMDS_GAP_DELAY bound to: 6'b000000 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter CMD_WDT bound to: 1023 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter WR_WDT bound to: 8191 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter RD_WDT bound to: 1023 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PORT_MODE bound to: BI_MODE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CMD_PATTERN bound to: CGEN_ALL - type: string 
2default:defaulth p
x
� 
u
%s
*synth2]
I	Parameter ADDR_WIDTH_MASK bound to: 30'b000011111111111111111111111111 
2default:defaulth p
x
� 
w
%s
*synth2_
K	Parameter ADDR_WIDTH_MASK_1 bound to: 30'b000111111111111111111111111111 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter BEGIN_ADDRESS_MASK bound to: 0 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter END_ADDRESS_MASK bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SHIFT_COUNT bound to: 4 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter BEGIN_ADDRESS_INT bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter END_ADDRESS_INT bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter TG_INIT_DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter CMD_WDT_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter RD_WDT_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter WR_WDT_WIDTH bound to: 13 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter TG_FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys29
%mig_7series_v4_1_init_mem_pattern_ctr2default:default2
 2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
872default:default8@Z8-6157h px� 
b
%s
*synth2J
6	Parameter SIMULATION bound to: FALSE - type: string 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
s
%s
*synth2[
G	Parameter TST_MEM_INSTR_MODE bound to: R_W_INSTR_MODE - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CMD_PATTERN bound to: CGEN_ALL - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter BEGIN_ADDRESS bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter END_ADDRESS bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter CMD_SEED_VALUE bound to: 1447389059 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter DATA_SEED_VALUE bound to: 305419896 - type: integer 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PORT_MODE bound to: BI_MODE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter IDLE bound to: 8'b00000001 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter INIT_MEM_WRITE bound to: 8'b00000010 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter INIT_MEM_READ bound to: 8'b00000100 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter TEST_MEM bound to: 8'b00001000 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SINGLE_STEP_WRITE bound to: 8'b00010000 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter SINGLE_STEP_READ bound to: 8'b00100000 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter CMP_ERROR bound to: 8'b01000000 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter SINGLE_CMD_WAIT bound to: 8'b10000000 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter BRAM_ADDR bound to: 3'b000 
2default:defaulth p
x
� 
T
%s
*synth2<
(	Parameter FIXED_ADDR bound to: 3'b001 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter PRBS_ADDR bound to: 3'b010 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter SEQUENTIAL_ADDR bound to: 3'b011 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter BRAM_INSTR_MODE bound to: 4'b0000 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter FIXED_INSTR_MODE bound to: 4'b0001 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter R_W_INSTR_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter RP_WP_INSTR_MODE bound to: 4'b0011 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter R_RP_W_WP_INSTR_MODE bound to: 4'b0100 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter R_RP_W_WP_REF_INSTR_MODE bound to: 4'b0101 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter BRAM_BL_MODE bound to: 2'b00 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter FIXED_BL_MODE bound to: 2'b01 
2default:defaulth p
x
� 
U
%s
*synth2=
)	Parameter PRBS_BL_MODE bound to: 2'b10 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter BRAM_DATAL_MODE bound to: 4'b0000 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter FIXED_DATA_MODE bound to: 4'b0001 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ADDR_DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter HAMMER_DATA_MODE bound to: 4'b0011 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter NEIGHBOR_DATA_MODE bound to: 4'b0100 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING1_DATA_MODE bound to: 4'b0101 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING0_DATA_MODE bound to: 4'b0110 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter PRBS_DATA_MODE bound to: 4'b0111 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter RD_INSTR bound to: 3'b001 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter RDP_INSTR bound to: 3'b011 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter WR_INSTR bound to: 3'b000 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter WRP_INSTR bound to: 3'b010 
2default:defaulth p
x
� 
W
%s
*synth2?
+	Parameter REFRESH_INSTR bound to: 3'b100 
2default:defaulth p
x
� 
V
%s
*synth2>
*	Parameter NOP_WR_INSTR bound to: 3'b101 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
mode_load_d1_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
4832default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
mode_load_d2_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
4852default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
mode_load_d3_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
4872default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
mode_load_d4_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
4892default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
mode_load_d5_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
4912default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2'
mode_load_pulse_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
5012default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2

Cout_b_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
8442default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COuta_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
9982default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
pre_instr_switch_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
10462default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
switch_instr_reg2default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
11002default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys29
%mig_7series_v4_1_init_mem_pattern_ctr2default:default2
 2default:default2
22default:default2
12default:default2q
[/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_init_mem_pattern_ctr.v2default:default2
872default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys25
!mig_7series_v4_1_memc_traffic_gen2default:default2
 2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
812default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SIMULATION bound to: FALSE - type: string 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter tCK bound to: 2500 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PORT_MODE bound to: BI_MODE - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CMD_PATTERN bound to: CGEN_ALL - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter BANK_WIDTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CMP_DATA_PIPE_STAGES bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter MEM_COL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter PRBS_EADDR_MASK_POS bound to: -16777216 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter PRBS_SADDR_MASK_POS bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PRBS_EADDR bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_SADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys23
mig_7series_v4_1_read_data_path2default:default2
 2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
722default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter CMP_DATA_PIPE_STAGES bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter MEM_COL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SIMULATION bound to: FALSE - type: string 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter ER_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys25
!mig_7series_v4_1_read_posted_fifo2default:default2
 2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_posted_fifo.v2default:default2
692default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2*
mig_7series_v4_1_afifo2default:default2
 2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
692default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter DSIZE bound to: 42 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FIFO_DEPTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ASIZE bound to: 4 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter SYNC bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COuta_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
1582default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COutc_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
1972default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2*
mig_7series_v4_1_afifo2default:default2
 2default:default2
32default:default2
12default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
692default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys20
dfifo_has_enough_room_d1_reg2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_posted_fifo.v2default:default2
1632default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys25
!mig_7series_v4_1_read_posted_fifo2default:default2
 2default:default2
42default:default2
12default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_posted_fifo.v2default:default2
692default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
mig_7series_v4_1_rd_data_gen2default:default2
 2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_rd_data_gen.v2default:default2
692default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COLUMN_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2
 2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
852default:default8@Z8-6157h px� 
\
%s
*synth2D
0	Parameter DMODE bound to: READ - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COLUMN_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TAPS_VALUE bound to: 8'b10001110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter NUM_WIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter USER_BUS_DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter BRAM_DATAL_MODE bound to: 4'b0000 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter FIXED_DATA_MODE bound to: 4'b0001 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ADDR_DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter HAMMER_DATA_MODE bound to: 4'b0011 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter NEIGHBOR_DATA_MODE bound to: 4'b0100 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING1_DATA_MODE bound to: 4'b0101 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING0_DATA_MODE bound to: 4'b0110 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter PRBS_DATA_MODE bound to: 4'b0111 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys22
mig_7series_v4_1_data_prbs_gen2default:default2
 2default:default2j
T/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_data_prbs_gen.v2default:default2
702default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SEED_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys22
mig_7series_v4_1_data_prbs_gen2default:default2
 2default:default2
52default:default2
12default:default2j
T/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_data_prbs_gen.v2default:default2
702default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys20
mig_7series_v4_1_tg_prbs_gen2default:default2
 2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
822default:default8@Z8-6157h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1042default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter PRBS_SEQ_LEN_CYCLES bound to: 64 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter PRBS_SEQ_LEN_CYCLES_BITS bound to: 6 - type: integer 
2default:defaulth p
x
� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1042default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
1062default:default8@Z8-6104h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
mig_7series_v4_1_tg_prbs_gen2default:default2
 2default:default2
62default:default2
12default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_prbs_gen.v2default:default2
822default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
 2default:default2r
\/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_vio_init_pattern_bram.v2default:default2
702default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter ADDR_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter DEPTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
default block is never used226*oasys2r
\/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_vio_init_pattern_bram.v2default:default2
2842default:default8@Z8-226h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
 2default:default2
72default:default2
12default:default2r
\/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_vio_init_pattern_bram.v2default:default2
702default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2.
ADDRESS_PATTERN.COut_a_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
7952default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys24
 PSUEDO_PRBS_PATTERN.m_addr_r_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
9172default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2+
data_mode_rr_c_reg[3:0]2default:default2+
data_mode_rr_a_reg[3:0]2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
3082default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
data_mode_rr_c_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
3082default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2
 2default:default2
82default:default2
12default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
852default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
mig_7series_v4_1_rd_data_gen2default:default2
 2default:default2
92default:default2
12default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_rd_data_gen.v2default:default2
692default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2:
&mig_7series_v4_1_afifo__parameterized02default:default2
 2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
692default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter DSIZE bound to: 128 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FIFO_DEPTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter ASIZE bound to: 4 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter SYNC bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COuta_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
1582default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COutc_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
1972default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2:
&mig_7series_v4_1_afifo__parameterized02default:default2
 2default:default2
92default:default2
12default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
692default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys24
 gen_error_v7.dq_bit_error_r1_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
6952default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys25
!gen_error_v7.dq_lane_error_r2_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
6972default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
gen_error_v7.COuta_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
7372default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
wait_bl_end_r1_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
2452default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2'
force_wrcmd_gen_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
2542default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys20
force_wrcmd_timeout_cnts_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
2652default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2#
wait_bl_end_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
2752default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2)
v6_data_cmp_valid_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
4212default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2#
cmp_data_en_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
4252default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys23
mig_7series_v4_1_read_data_path2default:default2
 2default:default2
102default:default2
12default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_read_data_path.v2default:default2
722default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys24
 mig_7series_v4_1_write_data_path2default:default2
 2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_write_data_path.v2default:default2
702default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter MEM_COL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys20
mig_7series_v4_1_wr_data_gen2default:default2
 2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_wr_data_gen.v2default:default2
702default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter MODE bound to: WR - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COLUMN_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2
 2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
852default:default8@Z8-6157h px� 
]
%s
*synth2E
1	Parameter DMODE bound to: WRITE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter EYE_TEST bound to: FALSE - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter START_ADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter COLUMN_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter SEL_VICTIM_LINE bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter TAPS_VALUE bound to: 8'b10001110 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter NUM_WIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter USER_BUS_DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter BRAM_DATAL_MODE bound to: 4'b0000 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter FIXED_DATA_MODE bound to: 4'b0001 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ADDR_DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter HAMMER_DATA_MODE bound to: 4'b0011 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter NEIGHBOR_DATA_MODE bound to: 4'b0100 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING1_DATA_MODE bound to: 4'b0101 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING0_DATA_MODE bound to: 4'b0110 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter PRBS_DATA_MODE bound to: 4'b0111 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys2.
ADDRESS_PATTERN.COut_a_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
7952default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys24
 PSUEDO_PRBS_PATTERN.m_addr_r_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
9172default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2+
data_mode_rr_c_reg[3:0]2default:default2+
data_mode_rr_a_reg[3:0]2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
3082default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
data_mode_rr_c_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
3082default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys22
user_burst_cnt_larger_bram_reg2default:default28
$wr_ubr.user_burst_cnt_larger_1_r_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
4692default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys22
user_burst_cnt_larger_bram_reg2default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
4692default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2
 2default:default2
102default:default2
12default:default2k
U/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_s7ven_data_gen.v2default:default2
852default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2"
cmd_startF_reg2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_wr_data_gen.v2default:default2
2052default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2 
cmd_rdyF_reg2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_wr_data_gen.v2default:default2
3542default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys20
mig_7series_v4_1_wr_data_gen2default:default2
 2default:default2
112default:default2
12default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_wr_data_gen.v2default:default2
702default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys24
 mig_7series_v4_1_write_data_path2default:default2
 2default:default2
122default:default2
12default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_write_data_path.v2default:default2
702default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2,
mig_7series_v4_1_cmd_gen2default:default2
 2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
1012default:default8@Z8-6157h px� 
j
Hattribute "use_dsp48" has been deprecated, please use "use_dsp" instead 4323*oasysZ8-5974h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter PORT_MODE bound to: BI_MODE - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter DATA_PATTERN bound to: DGEN_ALL - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter CMD_PATTERN bound to: CGEN_ALL - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 27 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter BANK_WIDTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PIPE_STAGES bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter MEM_COL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter PRBS_EADDR_MASK_POS bound to: -16777216 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter PRBS_SADDR_MASK_POS bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PRBS_EADDR bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_SADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter PRBS_ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter INSTR_PRBS_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter BL_PRBS_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter BRAM_DATAL_MODE bound to: 4'b0000 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	Parameter FIXED_DATA_MODE bound to: 4'b0001 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter ADDR_DATA_MODE bound to: 4'b0010 
2default:defaulth p
x
� 
[
%s
*synth2C
/	Parameter HAMMER_DATA_MODE bound to: 4'b0011 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter NEIGHBOR_DATA_MODE bound to: 4'b0100 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING1_DATA_MODE bound to: 4'b0101 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter WALKING0_DATA_MODE bound to: 4'b0110 
2default:defaulth p
x
� 
Y
%s
*synth2A
-	Parameter PRBS_DATA_MODE bound to: 4'b0111 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter DWIDTH_BY_8 bound to: 16 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter LOGB2_MEM_BURST_INT bound to: 3 - type: integer 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter BRAM_ADDR bound to: 2'b00 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter FIXED_ADDR bound to: 2'b01 
2default:defaulth p
x
� 
R
%s
*synth2:
&	Parameter PRBS_ADDR bound to: 2'b10 
2default:defaulth p
x
� 
X
%s
*synth2@
,	Parameter SEQUENTIAL_ADDR bound to: 2'b11 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_INT bound to: 8 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys21
mig_7series_v4_1_cmd_prbs_gen2default:default2
 2default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_CMD bound to: ADDRESS - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SEED_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
p
%s
*synth2X
D	Parameter PRBS_EADDR_MASK_POS bound to: -16777216 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter PRBS_SADDR_MASK_POS bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter PRBS_EADDR bound to: 16777215 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_SADDR bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys21
mig_7series_v4_1_cmd_prbs_gen2default:default2
 2default:default2
132default:default2
12default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2A
-mig_7series_v4_1_cmd_prbs_gen__parameterized02default:default2
 2default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter FAMILY bound to: SPARTAN6 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_CMD bound to: INSTR - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SEED_WIDTH bound to: 15 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter PRBS_EADDR_MASK_POS bound to: -12288 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter PRBS_SADDR_MASK_POS bound to: 8192 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_EADDR bound to: 8192 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_SADDR bound to: 8192 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2A
-mig_7series_v4_1_cmd_prbs_gen__parameterized02default:default2
 2default:default2
132default:default2
12default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2A
-mig_7series_v4_1_cmd_prbs_gen__parameterized12default:default2
 2default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter FAMILY bound to: SPARTAN6 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 29 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_CMD bound to: INSTR - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SEED_WIDTH bound to: 15 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter PRBS_EADDR_MASK_POS bound to: -12288 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter PRBS_SADDR_MASK_POS bound to: 8192 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_EADDR bound to: 8192 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_SADDR bound to: 8192 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2A
-mig_7series_v4_1_cmd_prbs_gen__parameterized12default:default2
 2default:default2
132default:default2
12default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2A
-mig_7series_v4_1_cmd_prbs_gen__parameterized22default:default2
 2default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter PRBS_CMD bound to: BLEN - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter PRBS_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter SEED_WIDTH bound to: 15 - type: integer 
2default:defaulth p
x
� 
m
%s
*synth2U
A	Parameter PRBS_EADDR_MASK_POS bound to: -12288 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter PRBS_SADDR_MASK_POS bound to: 8192 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_EADDR bound to: 8192 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter PRBS_SADDR bound to: 8192 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2A
-mig_7series_v4_1_cmd_prbs_gen__parameterized22default:default2
 2default:default2
132default:default2
12default:default2i
S/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_prbs_gen.v2default:default2
712default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2+
seq_addr_gen.COut_c_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
12392default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
bl_out_clk_en_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
4862default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2"
bl_out_vld_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
4882default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2'
force_wrcmd_gen_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
6702default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
instr_vld_dly1_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
8162default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
rd_data_counts_asked_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
8282default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COutA_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
8342default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2/
rd_data_received_counts_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
8522default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2#
buf_avail_r_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
8762default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
n_gen_write_only_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
12582default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
refresh_timer_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
13842default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
refresh_cmd_en_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
13972default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2"
bl_out_reg_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
16342default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2
cmd_vld_reg2default:default2!
instr_vld_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
5222default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
cmd_vld_reg2default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
5222default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2,
mig_7series_v4_1_cmd_gen2default:default2
 2default:default2
142default:default2
12default:default2d
N/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_cmd_gen.v2default:default2
1012default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys27
#mig_7series_v4_1_memc_flow_vcontrol2default:default2
 2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
702default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter nCK_PER_CLK bound to: 2 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter NUM_DQ_PINS bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BL_WIDTH bound to: 10 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter MEM_BURST_LEN bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter FAMILY bound to: VIRTEX6 - type: string 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MEM_TYPE bound to: DDR3 - type: string 
2default:defaulth p
x
� 
P
%s
*synth28
$	Parameter READY bound to: 4'b0001 
2default:defaulth p
x
� 
O
%s
*synth27
#	Parameter READ bound to: 4'b0010 
2default:defaulth p
x
� 
P
%s
*synth28
$	Parameter WRITE bound to: 4'b0100 
2default:defaulth p
x
� 
S
%s
*synth2;
'	Parameter CMD_WAIT bound to: 4'b1000 
2default:defaulth p
x
� 
L
%s
*synth24
 	Parameter RD bound to: 3'b001 
2default:defaulth p
x
� 
M
%s
*synth25
!	Parameter RDP bound to: 3'b011 
2default:defaulth p
x
� 
L
%s
*synth24
 	Parameter WR bound to: 3'b000 
2default:defaulth p
x
� 
M
%s
*synth25
!	Parameter WRP bound to: 3'b010 
2default:defaulth p
x
� 
Q
%s
*synth29
%	Parameter REFRESH bound to: 3'b100 
2default:defaulth p
x
� 
M
%s
*synth25
!	Parameter NOP bound to: 3'b101 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys2!
cmd_en_r2_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2082default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COuta_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2672default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2!
cmd_rdy_o_reg2default:default2"
push_cmd_r_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
1902default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys2$
wdp_validB_o_reg2default:default2#
wdp_valid_o_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2802default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys2$
wdp_validC_o_reg2default:default2#
wdp_valid_o_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2812default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2!
cmd_rdy_o_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
1902default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
wdp_validB_o_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2802default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2$
wdp_validC_o_reg2default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2812default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys27
#mig_7series_v4_1_memc_flow_vcontrol2default:default2
 2default:default2
152default:default2
12default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
702default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2.
mig_7series_v4_1_tg_status2default:default2
 2default:default2f
P/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_status.v2default:default2
702default:default8@Z8-6157h px� 
Z
%s
*synth2B
.	Parameter TCQ bound to: 100 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter DWIDTH bound to: 128 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2.
mig_7series_v4_1_tg_status2default:default2
 2default:default2
162default:default2
12default:default2f
P/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_tg_status.v2default:default2
702default:default8@Z8-6155h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COutc_reg2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
4292default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	COutd_reg2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
4382default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2*
data_mode_r_b_reg[3:0]2default:default2*
data_mode_r_a_reg[3:0]2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
3952default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys2*
data_mode_r_c_reg[3:0]2default:default2*
data_mode_r_a_reg[3:0]2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
3962default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
data_mode_r_b_reg2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
3952default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
data_mode_r_c_reg2default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
3962default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys25
!mig_7series_v4_1_memc_traffic_gen2default:default2
 2default:default2
172default:default2
12default:default2m
W/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_traffic_gen.v2default:default2
812default:default8@Z8-6155h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2692default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2692default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2692default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
2712default:default8@Z8-6104h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys24
 mig_7series_v4_1_traffic_gen_top2default:default2
 2default:default2
182default:default2
12default:default2l
V/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_traffic_gen_top.v2default:default2
782default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
282default:default2!
memc_cmd_addr2default:default2
322default:default24
 mig_7series_v4_1_traffic_gen_top2default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
5042default:default8@Z8-689h px� 
�
&Input port '%s' has an internal driver4442*oasys2
size2default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
2672default:default8@Z8-6104h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2&
manual_clear_error2default:default2
example_top2default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
4982default:default8@Z8-3848h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
example_top2default:default2
 2default:default2
192default:default2
12default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
752default:default8@Z8-6155h px� 
�
!design %s has unconnected port %s3331*oasys27
#mig_7series_v4_1_memc_flow_vcontrol2default:default2!
mcb_wr_full_i2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys27
#mig_7series_v4_1_memc_flow_vcontrol2default:default2
mcb_wr_en_i2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2,
mig_7series_v4_1_cmd_gen2default:default2
rst_i[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2,
mig_7series_v4_1_cmd_gen2default:default2
rst_i[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2,
mig_7series_v4_1_cmd_gen2default:default2
rst_i[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2,
mig_7series_v4_1_cmd_gen2default:default2
rst_i[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2,
mig_7series_v4_1_cmd_gen2default:default2%
reading_rd_data_i2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2 
cmd_addr[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2:
&mig_7series_v4_1_vio_init_pattern_bram2default:default2
cmd_addr[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2$
prbs_fseed_i[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2C
/mig_7series_v4_1_s7ven_data_gen__parameterized02default:default2#
prbs_fseed_i[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
mig_7series_v4_1_wr_data_gen2default:default2
rst_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
mig_7series_v4_1_wr_data_gen2default:default2
rst_i[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
mig_7series_v4_1_wr_data_gen2default:default2
rst_i[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
mig_7series_v4_1_wr_data_gen2default:default2#
memc_cmd_full_i2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys24
 mig_7series_v4_1_write_data_path2default:default2
rst_i[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys24
 mig_7series_v4_1_write_data_path2default:default2
rst_i[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys24
 mig_7series_v4_1_write_data_path2default:default2
rst_i[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys24
 mig_7series_v4_1_write_data_path2default:default2
rst_i[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys24
 mig_7series_v4_1_write_data_path2default:default2
rst_i[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
mig_7series_v4_1_s7ven_data_gen2default:default2$
prbs_fseed_i[11]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
�Finished RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:04 . Memory (MB): peak = 1427.699 ; gain = 138.473 ; free physical = 1917 ; free virtual = 6349
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
�
'tying undriven pin %s:%s to constant 0
3295*oasys2%
u_traffic_gen_top2default:default2&
manual_clear_error2default:default2W
A/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.v2default:default2
4932default:default8@Z8-3295h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:03 ; elapsed = 00:00:04 . Memory (MB): peak = 1427.699 ; gain = 138.473 ; free physical = 1925 ; free virtual = 6357
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:03 ; elapsed = 00:00:04 . Memory (MB): peak = 1427.699 ; gain = 138.473 ; free physical = 1925 ; free virtual = 6357
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
Loading part %s157*device2$
xc7a100tcsg324-12default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�/home/davide/vivadoPrjects/mig_7series_0_ex/mig_7series_0_ex.srcs/sources_1/ip/mig_7series_0/mig_7series_0/mig_7series_0_in_context.xdc2default:default2%
u_mig_7series_0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�/home/davide/vivadoPrjects/mig_7series_0_ex/mig_7series_0_ex.srcs/sources_1/ip/mig_7series_0/mig_7series_0/mig_7series_0_in_context.xdc2default:default2%
u_mig_7series_0	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2Y
C/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2Y
C/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.xdc2default:default8Z20-178h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2W
C/home/davide/vivadoPrjects/mig_7series_0_ex/imports/example_top.xdc2default:default21
.Xil/example_top_propImpl.xdc2default:defaultZ1-236h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common24
 Constraint Validation Runtime : 2default:default2
00:00:00.012default:default2
00:00:00.012default:default2
1822.4962default:default2
0.0002default:default2
16252default:default2
60572default:defaultZ17-722h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:13 ; elapsed = 00:00:23 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1732 ; free virtual = 6164
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Loading part: xc7a100tcsg324-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:13 ; elapsed = 00:00:23 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1732 ; free virtual = 6164
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:13 ; elapsed = 00:00:23 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1734 ; free virtual = 6166
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2%
current_state_reg2default:default29
%mig_7series_v4_1_init_mem_pattern_ctr2default:defaultZ8-802h px� 
{
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
run_traffic2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
rd_ptr_cp_reg_rep2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
1412default:default8@Z8-6014h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
	wait_done2default:default2
22default:default2
52default:defaultZ8-5544h px� 
}
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2!
reseed_prbs_r2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
w1data2default:default2
42default:default2
52default:defaultZ8-5544h px� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2-
user_burst_cnt_larger_1_r2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
user_bl_cnt_is_12default:defaultZ8-5546h px� 
~
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2"
user_burst_cnt2default:defaultZ8-5546h px� 
x
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
prefetch2default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2%
rd_ptr_cp_reg_rep2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
1412default:default8@Z8-6014h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
w1data2default:default2
42default:default2
52default:defaultZ8-5544h px� 
~
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2"
user_burst_cnt2default:defaultZ8-5546h px� 
w
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
cmd_rdy2default:defaultZ8-5546h px� 
�
RFound unconnected internal register '%s' and it is trimmed from '%s' to '%s' bits.3455*oasys2
cmd_reg_reg2default:default2
32default:default2
12default:default2o
Y/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_memc_flow_vcontrol.v2default:default2
2922default:default8@Z8-3936h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2%
current_state_reg2default:default27
#mig_7series_v4_1_memc_flow_vcontrol2default:defaultZ8-802h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
addr_INC2default:default2
42default:default2
52default:defaultZ8-5544h px� 
w
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
goahead2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

next_state2default:default2
12default:default2
52default:defaultZ8-5544h px� 
y
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
	rst_remem2default:defaultZ8-5546h px� 
�
6No Re-encoding of one hot register '%s' in module '%s'3445*oasys2%
current_state_reg2default:default2 
fsm22C99DE002default:defaultZ8-3898h px� 
�
6No Re-encoding of one hot register '%s' in module '%s'3445*oasys2%
current_state_reg2default:default2 
fsm192731E002default:defaultZ8-3898h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:15 ; elapsed = 00:00:25 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1721 ; free virtual = 6154
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     36 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     33 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     25 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     24 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 64    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 65    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      5 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit         XORs := 256   
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               64 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               36 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 41    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               26 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               25 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               13 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 128   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 65    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 21    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 316   
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               2K Bit         RAMs := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              672 Bit         RAMs := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit         RAMs := 8     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input    128 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    127 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     46 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     45 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     36 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 24    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 16    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     10 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 195   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 45    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 220   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
Z
%s
*synth2B
.Module mig_7series_v4_1_init_mem_pattern_ctr 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     25 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               25 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 20    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
K
%s
*synth23
Module mig_7series_v4_1_afifo 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              672 Bit         RAMs := 1     
2default:defaulth p
x
� 
V
%s
*synth2>
*Module mig_7series_v4_1_read_posted_fifo 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      7 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                7 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module mig_7series_v4_1_data_prbs_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
[
%s
*synth2C
/Module mig_7series_v4_1_vio_init_pattern_bram 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 6     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit         RAMs := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module mig_7series_v4_1_tg_prbs_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit         XORs := 4     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module mig_7series_v4_1_s7ven_data_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     36 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               64 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               36 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input    128 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    127 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     36 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module mig_7series_v4_1_rd_data_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
[
%s
*synth2C
/Module mig_7series_v4_1_afifo__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               2K Bit         RAMs := 1     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module mig_7series_v4_1_read_data_path 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 26    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 147   
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
d
%s
*synth2L
8Module mig_7series_v4_1_s7ven_data_gen__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              128 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               64 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input    128 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input    127 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module mig_7series_v4_1_wr_data_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 13    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 24    
2default:defaulth p
x
� 
R
%s
*synth2:
&Module mig_7series_v4_1_cmd_prbs_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
b
%s
*synth2J
6Module mig_7series_v4_1_cmd_prbs_gen__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
b
%s
*synth2J
6Module mig_7series_v4_1_cmd_prbs_gen__parameterized1 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
b
%s
*synth2J
6Module mig_7series_v4_1_cmd_prbs_gen__parameterized2 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
M
%s
*synth25
!Module mig_7series_v4_1_cmd_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     33 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     24 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               26 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               13 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 19    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     46 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     45 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
X
%s
*synth2@
,Module mig_7series_v4_1_memc_flow_vcontrol 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      6 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               11 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 12    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 9     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 22    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
O
%s
*synth27
#Module mig_7series_v4_1_tg_status 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	              320 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
V
%s
*synth2>
*Module mig_7series_v4_1_memc_traffic_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
U
%s
*synth2=
)Module mig_7series_v4_1_traffic_gen_top 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                9 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2k
WPart Resources:
DSPs: 240 (col length:80)
BRAMs: 270 (col length: RAMB18 80 RAMB36 40)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
^ROM "%s" won't be mapped to RAM because address size (%s) is larger than maximum supported(%s)3997*oasys2-
user_burst_cnt_larger_1_r2default:default2
322default:default2
252default:defaultZ8-5545h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2R
>RD_PATH.read_data_path/read_postedfifo/rd_fifo/almost_full_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
2242default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2J
6RD_PATH.read_data_path/rd_datagen/user_bl_cnt_is_1_reg2default:default2h
R/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_rd_data_gen.v2default:default2
1532default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2A
-RD_PATH.read_data_path/rd_mdata_fifo/full_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
2132default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2@
,RD_PATH.read_data_path/rd_mdata_fifo/mem_reg2default:defaultZ8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2H
4RD_PATH.read_data_path/rd_mdata_fifo/almost_full_reg2default:default2b
L/home/davide/vivadoPrjects/mig_7series_0_ex/imports/mig_7series_v4_1_afifo.v2default:default2
2242default:default8@Z8-6014h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2F
2RD_PATH.read_data_path/rd_datagen/user_bl_cnt_is_12default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2?
+WR_PATH.write_data_path/wr_data_gen/cmd_rdy2default:defaultZ8-5546h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
v\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
v\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[1] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[2]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
v\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[4]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[5]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
v\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[6] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[7]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[8]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[9]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[6]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
w\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[10] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[11]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[12]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[13]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[14]2default:default2
FDRE2default:default2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[10]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
w\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[15] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[16]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[17]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[18]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[19]2default:default2
FDRE2default:default2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[10]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
y\u_traffic_gen_top/u_memc_traffic_gen /\WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen /\wdatamask_ripplecnt_reg[16] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[20]2default:default2
FDRE2default:default2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[15]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
y\u_traffic_gen_top/u_memc_traffic_gen /\WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen /\wdatamask_ripplecnt_reg[17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
w\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[21] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[18]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[22]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
y\u_traffic_gen_top/u_memc_traffic_gen /\WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen /\wdatamask_ripplecnt_reg[19] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[23]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[20]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[24]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[21]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[25]2default:default2
FDRE2default:default2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[10]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
y\u_traffic_gen_top/u_memc_traffic_gen /\WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen /\wdatamask_ripplecnt_reg[22] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[26]2default:default2
FDRE2default:default2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[15]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[23]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[27]2default:default2
FDRE2default:default2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[21]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[24]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[19]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
w\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\wdatamask_ripplecnt_reg[28] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[25]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[22]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
H\u_traffic_gen_top/u_init_mem_pattern_ctr/slow_write_read_button_r1_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Z
Fu_traffic_gen_top/u_init_mem_pattern_ctr/slow_write_read_button_r2_reg2default:default2
FDR2default:default2Z
Fu_traffic_gen_top/u_init_mem_pattern_ctr/slow_write_read_button_r1_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
~\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[22] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[23] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[20] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[21] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[18] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[19] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[30] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[31] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
{\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/wr_en_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2}
iu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/tg_st_addr_o_reg[9]2default:default2
FDE2default:default2�
{u_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/vio_init_pattern_bram/cmd_addr_r9_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[28] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[29] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[26] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[27] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[24] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2�
\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\vio_init_pattern_bram/hdata_reg[25] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[29]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
y\u_traffic_gen_top/u_memc_traffic_gen /\WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen /\wdatamask_ripplecnt_reg[26] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
�\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/read_postedfifo/rd_data_counts_asked0_inferred__0 /\RD_PATH.read_data_path/read_postedfifo/rd_data_counts_asked_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Q
=\u_traffic_gen_top/u_init_mem_pattern_ctr/bl_mode_sel_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2Q
=\u_traffic_gen_top/u_init_mem_pattern_ctr/bl_mode_sel_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
qu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[30]2default:default2
FDRE2default:default2�
pu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/wdatamask_ripplecnt_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[27]2default:default2
FDRE2default:default2�
su_traffic_gen_top/u_memc_traffic_gen/WR_PATH.write_data_path/wr_data_gen/s7ven_data_gen/wdatamask_ripplecnt_reg[17]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2^
J\u_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_data_mode_value_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_data_mode_value_reg[1]2default:default2
FDSE2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_data_mode_value_reg[2]2default:default2
FDRE2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_data_mode_value_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_data_mode_value_reg[3]2default:default2
FDRE2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2`
Lu_traffic_gen_top/u_memc_traffic_gen/u_c_gen/INC_COUNTS_V.INC_COUNTS_reg[12]2default:default2
FDR2default:default2Q
=u_traffic_gen_top/u_memc_traffic_gen/u_c_gen/cal_blout_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2_
Ku_traffic_gen_top/u_memc_traffic_gen/u_c_gen/INC_COUNTS_V.INC_COUNTS_reg[0]2default:default2
FD2default:default2Q
=u_traffic_gen_top/u_memc_traffic_gen/u_c_gen/instr_out_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2_
Ku_traffic_gen_top/u_memc_traffic_gen/u_c_gen/INC_COUNTS_V.INC_COUNTS_reg[1]2default:default2
FD2default:default2Q
=u_traffic_gen_top/u_memc_traffic_gen/u_c_gen/instr_out_reg[2]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Q
=\u_traffic_gen_top/u_init_mem_pattern_ctr/addr_mode_o_reg[2] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[0]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[1]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[2]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[3]2default:default2
FD2default:default2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/data_mode_o_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[4]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[5]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[6]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[7]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[8]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2[
Gu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[9]2default:default2
FD2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/INC_COUNTS_V.INC_COUNTS_reg[10]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[8]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[9]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[7]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[7]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[4]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[2]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[1]2default:default2
FD2default:default2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/fix_bl_value_reg[0]2default:default2
FD2default:default2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/data_mode_o_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2N
:\u_traffic_gen_top/u_memc_traffic_gen /\end_addr_r_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[1]2default:default2
FDSE2default:default2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[0]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2^
J\u_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2^
J\u_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[6]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[7]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[0]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[1]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[2]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[3]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[4]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[14]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[5]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[15]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[14]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[15]2default:default2
FDSE2default:default2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[8]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
tu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[9]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[10]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[12]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[11]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[13]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[12]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[22]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[13]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[23]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[22]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[16]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[23]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[16]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[18]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[17]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[18]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[20]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[19]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[21]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[20]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[30]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[21]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[31]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[30]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[24]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[31]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[25]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[24]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[26]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[25]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[27]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[26]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[28]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[27]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[29]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[28]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[71]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[29]2default:default2
FDSE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[65]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[38]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[32]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[39]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[33]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[32]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[34]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[33]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[35]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[34]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[36]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[35]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[37]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[36]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[46]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[37]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[47]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[46]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[40]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[47]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[41]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[40]2default:default2
FDRE2default:default2�
uu_traffic_gen_top/u_memc_traffic_gen/RD_PATH.read_data_path/rd_datagen/s7ven_data_gen/calib_data32.calib_data_reg[42]2default:defaultZ8-3886h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-38862default:default2
1002default:defaultZ17-14h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[6] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[7] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[4] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[5] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[14] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[15] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[8] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2|
h\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[9] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[10] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[11] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[12] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[13] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[22] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[23] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[16] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[17] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[18] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[19] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[20] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[21] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[30] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[31] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[24] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[25] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[26] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[27] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[28] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[29] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[38] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[39] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
12default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[32] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[33] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[34] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[35] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[36] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[37] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[46] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[47] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[40] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[41] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2}
i\u_traffic_gen_top/u_memc_traffic_gen /\RD_PATH.read_data_path/rd_datagen/s7ven_data_gen /\hdata_reg[42] 2default:defaultZ8-3333h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33332default:default2
1002default:defaultZ17-14h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Q
=u_traffic_gen_top/u_init_mem_pattern_ctr/current_state_reg[7]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Q
=u_traffic_gen_top/u_init_mem_pattern_ctr/current_state_reg[6]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Q
=u_traffic_gen_top/u_init_mem_pattern_ctr/current_state_reg[5]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Q
=u_traffic_gen_top/u_init_mem_pattern_ctr/current_state_reg[4]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Q
=u_traffic_gen_top/u_init_mem_pattern_ctr/current_state_reg[2]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_data_mode_value_reg[0]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Q
=u_traffic_gen_top/u_init_mem_pattern_ctr/data_mode_sel_reg[0]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/bl_mode_sel_reg[1]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/bl_mode_sel_reg[0]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
Fu_traffic_gen_top/u_init_mem_pattern_ctr/slow_write_read_button_r1_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
Iu_traffic_gen_top/u_init_mem_pattern_ctr/toggle_start_stop_write_read_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/single_read_r1_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/single_read_r2_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/single_write_r1_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2P
<u_traffic_gen_top/u_init_mem_pattern_ctr/single_write_r2_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Y
Eu_traffic_gen_top/u_init_mem_pattern_ctr/single_instr_run_trarric_reg2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2O
;u_traffic_gen_top/u_init_mem_pattern_ctr/addr_mode_o_reg[2]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[0]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2\
Hu_traffic_gen_top/u_init_mem_pattern_ctr/syn1_vio_addr_mode_value_reg[2]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2V
Bu_traffic_gen_top/u_init_mem_pattern_ctr/end_boundary_addr_reg[24]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2V
Bu_traffic_gen_top/u_init_mem_pattern_ctr/end_boundary_addr_reg[23]2default:default2
example_top2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[32]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[31]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[30]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[29]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[28]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[27]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[26]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[25]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[24]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[23]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[22]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[21]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[20]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[19]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[18]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[17]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[16]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[15]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[14]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[13]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[12]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[11]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2D
0PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[10]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[9]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[8]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[7]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[6]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[5]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[4]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2C
/PSUEDO_PRBS_PATTERN.data_prbs_gen/lfsr_q_reg[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys28
$vio_init_pattern_bram/wr_addr_reg[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys28
$vio_init_pattern_bram/wr_addr_reg[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys28
$vio_init_pattern_bram/wr_addr_reg[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys28
$vio_init_pattern_bram/wr_addr_reg[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2=
)vio_init_pattern_bram/bram_rd_valid_o_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys28
$vio_init_pattern_bram/rd_addr_reg[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2<
(vio_init_pattern_bram/rd_addr_reg_rep[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2:
&vio_init_pattern_bram/mode_load_r1_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2:
&vio_init_pattern_bram/mode_load_r2_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys28
$vio_init_pattern_bram/init_write_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys23
vio_init_pattern_bram/wr_en_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/reseed_prbs_r_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[7]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[6]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[5]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[4]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[0].u_data_prbs_gen/lfsr_reg_r_reg[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/reseed_prbs_r_reg2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[7]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[6]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[5]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[4]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[3]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[2]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[1]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2I
5gen_prbs_modules[1].u_data_prbs_gen/lfsr_reg_r_reg[0]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2K
7gen_prbs_modules[1].u_data_prbs_gen/reseed_cnt_r_reg[7]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2K
7gen_prbs_modules[1].u_data_prbs_gen/reseed_cnt_r_reg[6]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2K
7gen_prbs_modules[1].u_data_prbs_gen/reseed_cnt_r_reg[5]2default:default23
mig_7series_v4_1_s7ven_data_gen2default:defaultZ8-3332h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:20 ; elapsed = 00:00:31 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1698 ; free virtual = 6138
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
k
%s*synth2S
?
Distributed RAM: Preliminary Mapping  Report (see note below)
2default:defaulth px� 
�
%s*synth2�
�+---------------------------------------+--------------------------------------------------------+-----------+----------------------+--------------+
2default:defaulth px� 
�
%s*synth2�
�|Module Name                            | RTL Object                                             | Inference | Size (Depth x Width) | Primitives   | 
2default:defaulth px� 
�
%s*synth2�
�+---------------------------------------+--------------------------------------------------------+-----------+----------------------+--------------+
2default:defaulth px� 
�
%s*synth2�
�|\u_traffic_gen_top/u_memc_traffic_gen  | RD_PATH.read_data_path/read_postedfifo/rd_fifo/mem_reg | Implied   | 16 x 42              | RAM32M x 7   | 
2default:defaulth px� 
�
%s*synth2�
�+---------------------------------------+--------------------------------------------------------+-----------+----------------------+--------------+

2default:defaulth px� 
�
%s*synth2�
�Note: The table above is a preliminary report that shows the Distributed RAMs at the current stage of the synthesis flow. Some Distributed RAMs may be reimplemented as non Distributed RAM primitives later in the synthesis flow. Multiple instantiated RAMs are reported only once.
2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
2Moved timing constraint from pin '%s' to pin '%s'
4028*oasys2*
u_mig_7series_0/ui_clk2default:default23
u_mig_7series_0/bbstub_ui_clk/O2default:defaultZ8-5578h px� 
�
SMoved %s constraints on hierarchical pins to their respective driving/loading pins
4235*oasys2
12default:defaultZ8-5819h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:26 ; elapsed = 00:00:39 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1582 ; free virtual = 6021
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:00:26 ; elapsed = 00:00:39 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(
Distributed RAM: Final Mapping  Report
2default:defaulth p
x
� 
�
%s
*synth2�
�+---------------------------------------+--------------------------------------------------------+-----------+----------------------+--------------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|Module Name                            | RTL Object                                             | Inference | Size (Depth x Width) | Primitives   | 
2default:defaulth p
x
� 
�
%s
*synth2�
�+---------------------------------------+--------------------------------------------------------+-----------+----------------------+--------------+
2default:defaulth p
x
� 
�
%s
*synth2�
�|\u_traffic_gen_top/u_memc_traffic_gen  | RD_PATH.read_data_path/read_postedfifo/rd_fifo/mem_reg | Implied   | 16 x 42              | RAM32M x 7   | 
2default:defaulth p
x
� 
�
%s
*synth2�
�+---------------------------------------+--------------------------------------------------------+-----------+----------------------+--------------+

2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:00:26 ; elapsed = 00:00:39 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
,Flop %s is being inverted and renamed to %s.3906*oasys2U
Au_traffic_gen_top/u_memc_traffic_gen/memc_control/wdp_valid_o_reg2default:default2Y
Eu_traffic_gen_top/u_memc_traffic_gen/memc_control/wdp_valid_o_reg_inv2default:defaultZ8-5365h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
O
%s
*synth27
#|      |BlackBox name |Instances |
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
O
%s
*synth27
#|1     |mig_7series_0 |         1|
2default:defaulth p
x
� 
O
%s
*synth27
#+------+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
K
%s*synth23
+------+--------------+------+
2default:defaulth px� 
K
%s*synth23
|      |Cell          |Count |
2default:defaulth px� 
K
%s*synth23
+------+--------------+------+
2default:defaulth px� 
K
%s*synth23
|1     |mig_7series_0 |     1|
2default:defaulth px� 
K
%s*synth23
|2     |CARRY4        |    71|
2default:defaulth px� 
K
%s*synth23
|3     |LUT1          |    22|
2default:defaulth px� 
K
%s*synth23
|4     |LUT2          |   183|
2default:defaulth px� 
K
%s*synth23
|5     |LUT3          |    74|
2default:defaulth px� 
K
%s*synth23
|6     |LUT4          |    48|
2default:defaulth px� 
K
%s*synth23
|7     |LUT5          |    72|
2default:defaulth px� 
K
%s*synth23
|8     |LUT6          |   182|
2default:defaulth px� 
K
%s*synth23
|9     |RAM32M        |     7|
2default:defaulth px� 
K
%s*synth23
|10    |FDRE          |   423|
2default:defaulth px� 
K
%s*synth23
|11    |FDSE          |    17|
2default:defaulth px� 
K
%s*synth23
|12    |OBUF          |     2|
2default:defaulth px� 
K
%s*synth23
+------+--------------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
�
%s
*synth2�
t+------+--------------------------------------------------+------------------------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2�
t|      |Instance                                          |Module                                          |Cells |
2default:defaulth p
x
� 
�
%s
*synth2�
t+------+--------------------------------------------------+------------------------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2�
t|1     |top                                               |                                                |  1281|
2default:defaulth p
x
� 
�
%s
*synth2�
t|2     |  u_traffic_gen_top                               |mig_7series_v4_1_traffic_gen_top                |  1099|
2default:defaulth p
x
� 
�
%s
*synth2�
t|3     |    u_init_mem_pattern_ctr                        |mig_7series_v4_1_init_mem_pattern_ctr           |   119|
2default:defaulth p
x
� 
�
%s
*synth2�
t|4     |    u_memc_traffic_gen                            |mig_7series_v4_1_memc_traffic_gen               |   966|
2default:defaulth p
x
� 
�
%s
*synth2�
t|5     |      \RD_PATH.read_data_path                     |mig_7series_v4_1_read_data_path                 |   331|
2default:defaulth p
x
� 
�
%s
*synth2�
t|6     |        rd_datagen                                |mig_7series_v4_1_rd_data_gen                    |   151|
2default:defaulth p
x
� 
�
%s
*synth2�
t|7     |          s7ven_data_gen                          |mig_7series_v4_1_s7ven_data_gen                 |   117|
2default:defaulth p
x
� 
�
%s
*synth2�
t|8     |            \gen_prbs_modules[0].u_data_prbs_gen  |mig_7series_v4_1_tg_prbs_gen                    |    23|
2default:defaulth p
x
� 
�
%s
*synth2�
t|9     |        read_postedfifo                           |mig_7series_v4_1_read_posted_fifo               |   138|
2default:defaulth p
x
� 
�
%s
*synth2�
t|10    |          rd_fifo                                 |mig_7series_v4_1_afifo                          |    83|
2default:defaulth p
x
� 
�
%s
*synth2�
t|11    |      \WR_PATH.write_data_path                    |mig_7series_v4_1_write_data_path                |   122|
2default:defaulth p
x
� 
�
%s
*synth2�
t|12    |        wr_data_gen                               |mig_7series_v4_1_wr_data_gen                    |   122|
2default:defaulth p
x
� 
�
%s
*synth2�
t|13    |          s7ven_data_gen                          |mig_7series_v4_1_s7ven_data_gen__parameterized0 |    77|
2default:defaulth p
x
� 
�
%s
*synth2�
t|14    |            vio_init_pattern_bram                 |mig_7series_v4_1_vio_init_pattern_bram          |     2|
2default:defaulth p
x
� 
�
%s
*synth2�
t|15    |      memc_control                                |mig_7series_v4_1_memc_flow_vcontrol             |   162|
2default:defaulth p
x
� 
�
%s
*synth2�
t|16    |      tg_status                                   |mig_7series_v4_1_tg_status                      |     3|
2default:defaulth p
x
� 
�
%s
*synth2�
t|17    |      u_c_gen                                     |mig_7series_v4_1_cmd_gen                        |   347|
2default:defaulth p
x
� 
�
%s
*synth2�
t|18    |        \gen_prbs_instr.instr_prbs_gen_a          |mig_7series_v4_1_cmd_prbs_gen__parameterized0   |    22|
2default:defaulth p
x
� 
�
%s
*synth2�
t+------+--------------------------------------------------+------------------------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1580 ; free virtual = 6019
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
u
%s
*synth2]
ISynthesis finished with 0 errors, 0 critical warnings and 3714 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:00:19 ; elapsed = 00:00:23 . Memory (MB): peak = 1822.496 ; gain = 138.473 ; free physical = 1632 ; free virtual = 6071
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:00:27 ; elapsed = 00:00:40 . Memory (MB): peak = 1822.496 ; gain = 533.270 ; free physical = 1632 ; free virtual = 6071
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
782default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
!Unisim Transformation Summary:
%s111*project2�
�  A total of 7 instances were transformed.
  RAM32M => RAM32M (RAMD32, RAMD32, RAMD32, RAMD32, RAMD32, RAMD32, RAMS32, RAMS32): 7 instances
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
3092default:default2
3492default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:00:292default:default2
00:00:412default:default2
1822.4962default:default2
533.4222default:default2
16302default:default2
60702default:defaultZ17-722h px� 
K
"No constraint will be written out.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2m
Y/home/davide/vivadoPrjects/mig_7series_0_ex/mig_7series_0_ex.runs/synth_1/example_top.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
lExecuting : report_utilization -file example_top_utilization_synth.rpt -pb example_top_utilization_synth.pb
2default:defaulth px� 
�
�report_utilization: Time (s): cpu = 00:00:00.09 ; elapsed = 00:00:00.10 . Memory (MB): peak = 1846.508 ; gain = 0.000 ; free physical = 1629 ; free virtual = 6068
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Thu Jul 11 10:51:40 20192default:defaultZ17-206h px� 


End Record